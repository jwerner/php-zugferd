ZUGFerd / X-Factur PHP Class
============================

This repository provides a PHP class to generate the XML for a ZUGFeRD / X-Factur XML file.

The XML file represents all the data required for an electronic invoice.

The electronic invoice within this context is a PDF document which has the invoice xml empeded as a file attachment.

The generated file complies to the norm EN 16931's schema FACTUR-X_EN16931.xsd (see Example/ant validation).

The PHP class assumes invoice data to be provided as a standard PHP object. See Example/invoice.php for details.

The PHP class is adopted from the original InvoicePlane class, to reflect the new ZUGFeRD version 2.1.


Class File
----------

* see src/ZugferdXml.php

Example
-------

Find an example in the example folder.

Files:

* zugferd_2p1_EN16931_Elektron.pdf: Master example invoice for the generated example XML
* factur-x.xml: Inline attachment, saved from zugferd_2p1_EN16931_Elektron.pdf

* invoice.php: Returns a PHP object with invoice master data (e.g. title, number, sums etc)
* items.php: Returns an array with invoice line items objects
* index.php: Instanciate ZugferdXml class with the sample data and save the generated XML file
* factur-x_test.xml: Generated with ZugferdXml.php from invoice.php and items.php

* build.xml: Validate the generated XML file against the schema FACTUR-X_EN16931.xsd, using [ant][ant]. The build script expects the ZUGFeRD specification in the sub-folder ``zugferd22de/DE/Schema/FACTUR-X_EN16931.xsd``

References
----------

* [ZUGFeRD](https://www.ferd-net.de/)
* [ZUGFeRD Standards](https://www.ferd-net.de/standards/zugferd/index.html)
* [ZUGFeRD Wikipedia](https://de.wikipedia.org/wiki/ZUGFeRD)
* [InvoicePlane/ZugferdXml](https://github.com/InvoicePlane/InvoicePlane/blob/master/application/libraries/ZugferdXml.php)

[ant]: https://ant.apache.org/manual/index.html
