<?php
/**
 * Class ZugferdXml
 *
 * Generates an XML for the ZUGFerD 2.1 /X-Factur / EN 16931 Electronic Invoice
 * @author InvoicePlane
 * @author Joachim Werner <joachim.werner@diggin-data.de>
 */
class ZugferdXml
{
    private $invoice;
    private $items;
    private $currencyCode;
    private $doc;
    private $root;

    public function __construct($params)
    { // {{{ 
        $this->invoice = $params['invoice'];
        $this->items = $params['items'];
        $this->currencyCode = $params['currency_code'];
    } // }}}

    public function xml()
    { // {{{ 
        $this->doc = new DOMDocument('1.0', 'UTF-8');
        $this->doc->formatOutput = true;

        $this->root = $this->xmlRoot();
        $this->root->appendChild($this->xmlExchangedDocumentContext());
        $this->root->appendChild($this->xmlExchangedDocument());
        $this->root->appendChild($this->xmlSupplyChainTradeTransaction());

        $this->doc->appendChild($this->root);
        return $this->doc->saveXML();
    } // }}}

    protected function xmlRoot()
    { // {{{ 
        $node = $this->doc->createElement('rsm:CrossIndustryInvoice');
        $node->setAttribute('xmlns:a', 'urn:un:unece:uncefact:data:standard:QualifiedDataType:100');
        $node->setAttribute('xmlns:rsm', 'urn:un:unece:uncefact:data:standard:CrossIndustryInvoice:100');
        $node->setAttribute('xmlns:qdt', 'urn:un:unece:uncefact:data:standard:QualifiedDataType:10');
        $node->setAttribute('xmlns:ram', 'urn:un:unece:uncefact:data:standard:ReusableAggregateBusinessInformationEntity:100');
        $node->setAttribute('xmlns:xs', 'http://www.w3.org/2001/XMLSchema');
        $node->setAttribute('xmlns:udt', 'urn:un:unece:uncefact:data:standard:UnqualifiedDataType:100');
        return $node;
    } // }}}

    protected function xmlExchangedDocument()
    { // {{{ 
        $node = $this->doc->createElement('rsm:ExchangedDocument');

        $node->appendChild($this->doc->createElement('ram:ID', $this->invoice->invoice_number));
        $node->appendChild($this->doc->createElement('ram:TypeCode', 204));
        
        echo 'IssueDateTime'.PHP_EOL;
        $dateNode = $this->doc->createElement('ram:IssueDateTime');
        $dateNode->appendChild($this->dateElement($this->invoice->invoice_date_created));
        $node->appendChild($dateNode);

        echo 'IncludedNote'.PHP_EOL;
        $noteNode = $this->doc->createElement('ram:IncludedNote');
        $noteNode->appendChild($this->doc->createElement('ram:Content', htmlsc($this->invoice->invoice_terms)));
        $node->appendChild($noteNode);

        return $node;
    } // }}}

    protected function xmlExchangedDocumentContext()
    { // {{{ 
        $node = $this->doc->createElement('rsm:ExchangedDocumentContext');
        
        $businessProcessNode = $this->doc->createElement('ram:BusinessProcessSpecifiedDocumentContextParameter');
        $businessProcessNode->appendChild($this->doc->createElement('ram:ID', $this->invoice->invoice_title));
        $node->appendChild($businessProcessNode);
        
        $guidelineNode = $this->doc->createElement('ram:GuidelineSpecifiedDocumentContextParameter');
        $guidelineNode->appendChild($this->doc->createElement('ram:ID', 'urn:cen.eu:en16931:2017'));
        $node->appendChild($guidelineNode);
        return $node;
    } // }}}

    protected function xmlSupplyChainTradeTransaction()
    { // {{{ 
        $node = $this->doc->createElement('rsm:SupplyChainTradeTransaction');
        foreach ($this->items as $index => $item) {
            $node->appendChild($this->xmlIncludedSupplyChainTradeLineItem($item->item_line_id, $item));
        }
        $node->appendChild($this->xmlApplicableHeaderTradeAgreement());
        $node->appendChild($this->xmlApplicableHeaderTradeDelivery());
        $node->appendChild($this->xmlApplicableHeaderTradeSettlement());
        return $node;
    } // }}}

    protected function xmlApplicableHeaderTradeAgreement()
    { // {{{ 
        $node = $this->doc->createElement('ram:ApplicableHeaderTradeAgreement');
        $node->appendChild($this->doc->createElement('ram:BuyerReference', $this->invoice->client_reference));
        $node->appendChild($this->xmlSellerTradeParty());
        $node->appendChild($this->xmlBuyerTradeParty());
        $node->appendChild($this->xmlSellerOrderReferencedDocument($this->invoice->user_order_reference));
        if(!empty($this->invoice->additional_referenced_documents)) {
            foreach($this->invoice->additional_referenced_documents as $additional_referenced_document) {
                $node->appendChild( $this->xmlAdditionalReferencedDocument(
                    $additional_referenced_document->issuerAssignedID, 
                    !empty($additional_referenced_document->uriID) ? $additional_referenced_document->uriID: null, 
                    !empty($additional_referenced_document->typeCode) ? $additional_referenced_document->typeCode: null
                ));
            }
        }
        return $node;
    } // }}}

    protected function xmlApplicableHeaderTradeDelivery()
    { // {{{ 
        $node = $this->doc->createElement('ram:ApplicableHeaderTradeDelivery');
        $node2 = $this->doc->createElement('ram:ActualDeliverySupplyChainEvent');
        $node3 = $this->doc->createElement('ram:OccurrenceDateTime');
        $node3->appendChild($this->dateElement($this->invoice->invoice_delivery_date));
        $node2->appendChild($node3);
        $node->appendChild($node2);
        return $node;    
    } // }}}

    protected function xmlSellerTradeParty($index = '', $item = '')
    { // {{{ 
        $node = $this->doc->createElement('ram:SellerTradeParty');
        $node->appendChild($this->doc->createElement('ram:ID', htmlsc($this->invoice->user_id)));
        $node->appendChild($this->doc->createElement('ram:Name', htmlsc($this->invoice->user_name)));
        $node->appendChild($this->doc->createElement('ram:Description', htmlsc($this->invoice->user_description)));

        // PostalTradeAddress
        $addressNode = $this->doc->createElement('ram:PostalTradeAddress');
        $addressNode->appendChild($this->doc->createElement('ram:PostcodeCode', htmlsc($this->invoice->user_zip)));
        $addressNode->appendChild($this->doc->createElement('ram:LineOne', htmlsc($this->invoice->user_address_1)));
        if(!empty($this->invoice->user_address_2))
            $addressNode->appendChild($this->doc->createElement('ram:LineTwo', htmlsc($this->invoice->user_address_2)));
        $addressNode->appendChild($this->doc->createElement('ram:CityName', htmlsc($this->invoice->user_city)));
        $addressNode->appendChild($this->doc->createElement('ram:CountryID', htmlsc($this->invoice->user_country)));

        $node->appendChild($addressNode);

        // SpecifiedTaxRegistration
        $node->appendChild($this->xmlSpecifiedTaxRegistration('VA', $this->invoice->user_vat_id));
        if(!empty($this->invoice->user_tax_code))
            $node->appendChild($this->xmlSpecifiedTaxRegistration('FC', htmlsc($this->invoice->user_tax_code)));

        return $node;
    } // }}}

    protected function xmlBuyerTradeParty($index = '', $item = '')
    { // {{{ 
        $node = $this->doc->createElement('ram:BuyerTradeParty');
        $node->appendChild($this->doc->createElement('ram:ID', htmlsc($this->invoice->client_id)));
        $node->appendChild($this->doc->createElement('ram:Name', $this->invoice->client_name));

        // PostalTradeAddress
        $addressNode = $this->doc->createElement('ram:PostalTradeAddress');
        $addressNode->appendChild($this->doc->createElement('ram:PostcodeCode', htmlsc($this->invoice->client_zip)));
        $addressNode->appendChild($this->doc->createElement('ram:LineOne', htmlsc($this->invoice->client_address_1)));
        if(!empty($this->invoice->client_address_2))
            $addressNode->appendChild($this->doc->createElement('ram:LineTwo', htmlsc($this->invoice->client_address_2)));
        $addressNode->appendChild($this->doc->createElement('ram:CityName', htmlsc($this->invoice->client_city)));
        $addressNode->appendChild($this->doc->createElement('ram:CountryID', htmlsc($this->invoice->client_country)));
        $node->appendChild($addressNode);

        // SpecifiedTaxRegistration
        if(!empty($this->invoice->client_vat_id))
            $node->appendChild($this->xmlSpecifiedTaxRegistration('VA', $this->invoice->client_vat_id));
        if(!empty($this->invoice->client_tax_code))
            $node->appendChild($this->xmlSpecifiedTaxRegistration('FC', htmlsc($this->invoice->client_tax_code)));

        return $node;
    } // }}}

    /**
     * @param string $schemeID
     */
    protected function xmlSpecifiedTaxRegistration($schemeID, $content)
    { // {{{
        $node = $this->doc->createElement('ram:SpecifiedTaxRegistration');
        $el = $this->doc->createElement('ram:ID', $content);
        $el->setAttribute('schemeID', $schemeID);
        $node->appendChild($el);
        return $node;
    } // }}}

    protected function xmlSellerOrderReferencedDocument($issuerAssignedID)
    { // {{{
        $node = $this->doc->createElement('ram:SellerOrderReferencedDocument');
        $el = $this->doc->createElement('ram:IssuerAssignedID', $issuerAssignedID);
        $node->appendChild($el);
        return $node;
    } // }}}

    protected function xmlAdditionalReferencedDocument($issuerAssignedID, $uriID=null, $typeCode=null)
    { // {{{
        $node = $this->doc->createElement('ram:AdditionalReferencedDocument');
        $el = $this->doc->createElement('ram:IssuerAssignedID', $issuerAssignedID);
        $node->appendChild($el);

        if(!empty($uriID)) {
            $el = $this->doc->createElement('ram:URIID', $uriID);
            $node->appendChild($el);
        }
        if(!empty($typeCode)) {
            $el = $this->doc->createElement('ram:TypeCode', $typeCode);
            $node->appendChild($el);
        }

        return $node;
    } // }}}

    protected function xmlIncludedSupplyChainTradeLineItem($lineNumber, $item)
    { // {{{ 
        $node = $this->doc->createElement('ram:IncludedSupplyChainTradeLineItem');

        // AssociatedDocumentLineDocument
        $lineNode = $this->doc->createElement('ram:AssociatedDocumentLineDocument');
        $lineNode->appendChild($this->doc->createElement('ram:LineID', $lineNumber));
        if(!empty($item->item_note)) {

            $noteNode = $this->doc->createElement('ram:IncludedNote');
            $noteNode->appendChild($this->doc->createElement('ram:Content', htmlsc($item->item_note)));
            $lineNode->appendChild($noteNode);
        }
        $node->appendChild($lineNode);

        // SpecifiedTradeProduct
        $tradeNode = $this->doc->createElement('ram:SpecifiedTradeProduct');
        $tradeNode->appendChild($this->doc->createElement('ram:Name', htmlsc($item->item_name) . "\n" . htmlsc($item->item_description)));
        $node->appendChild($tradeNode);

        // SpecifiedSupplyChainTradeAgreement
        $node->appendChild($this->xmlSpecifiedLineTradeAgreement($item));

        // SpecifiedSupplyChainTradeDelivery
        $deliveyNode = $this->doc->createElement('ram:SpecifiedLineTradeDelivery');
        $deliveyNode->appendChild($this->quantityElement('ram:BilledQuantity', $item->item_unit_code, $item->item_quantity));
        $node->appendChild($deliveyNode);

        // SpecifiedSupplyChainTradeSettlement
        $node->appendChild($this->xmlSpecifiedLineTradeSettlement($item));


        return $node;
    } // }}} 

    protected function xmlSpecifiedLineTradeAgreement($item)
    { // {{{
        $node = $this->doc->createElement('ram:SpecifiedLineTradeAgreement');

        // GrossPriceProductTradePrice
        $grossPriceNode = $this->doc->createElement('ram:GrossPriceProductTradePrice');
        $grossPriceNode->appendChild($this->currencyElement('ram:ChargeAmount', $item->item_price, 4));
        $node->appendChild($grossPriceNode);

        // NetPriceProductTradePrice
        $netPriceNode = $this->doc->createElement('ram:NetPriceProductTradePrice');
        $netPriceNode->appendChild($this->currencyElement('ram:ChargeAmount', $item->item_price, 4));
        $node->appendChild($netPriceNode);

        return $node;
    } // }}}

    protected function xmlSpecifiedLineTradeSettlement($item)
    { // {{{ 
        $node = $this->doc->createElement('ram:SpecifiedLineTradeSettlement');

        // ApplicableTradeTax
        if ($item->item_tax_rate_percent > 0) {
            $taxNode = $this->doc->createElement('ram:ApplicableTradeTax');
            $taxNode->appendChild($this->doc->createElement('ram:TypeCode', 'VAT'));
            $taxNode->appendChild($this->doc->createElement('ram:CategoryCode', 'S'));
            $taxNode->appendChild($this->doc->createElement('ram:RateApplicablePercent', $item->item_tax_rate_percent));
            $node->appendChild($taxNode);
        }

        // SpecifiedTradeSettlementLineMonetarySummation
        $sumNode = $this->doc->createElement('ram:SpecifiedTradeSettlementLineMonetarySummation');
        $sumNode->appendChild($this->currencyElement('ram:LineTotalAmount', $item->item_subtotal));
        $node->appendChild($sumNode);

        return $node;
    } // }}}

    protected function xmlApplicableHeaderTradeSettlement()
    { // {{{
        $node = $this->doc->createElement('ram:ApplicableHeaderTradeSettlement');

        $node->appendChild($this->doc->createElement('ram:PaymentReference', $this->invoice->invoice_title . ' ' . $this->invoice->invoice_number));
        $node->appendChild($this->doc->createElement('ram:InvoiceCurrencyCode', $this->currencyCode));

        // payment means
        if(!empty($this->invoice->user_payment_means)) {
            foreach($this->invoice->user_payment_means as $user_payment_means) {
                $node->appendChild($this->xmlSpecifiedTradeSettlementPaymentMeans($user_payment_means));
            }
        }
        
        // taxes
        foreach ($this->itemsSubtotalGroupedByTaxPercent() as $percent => $subtotal) {
            $node->appendChild($this->xmlApplicableTradeTax($percent, $subtotal));
        }

        // terms
        $nodeDescription = $this->doc->createElement('ram:Description', htmlsc($this->invoice->invoice_payment_terms));
        $nodePaymentTerms = $this->doc->createElement('ram:SpecifiedTradePaymentTerms');
        $nodePaymentTerms->appendChild($nodeDescription);
        $node->appendChild($nodePaymentTerms);

        // sums
        $node->appendChild($this->xmlSpecifiedTradeSettlementHeaderMonetarySummation());

        // ReceivableSpecifiedTradeAccountingAccount
        If(!empty($this->invoice->invoice_accounting_account)) {
            $nodeAccount = $this->doc->createElement('ram:ReceivableSpecifiedTradeAccountingAccount');
            $nodeAccount->appendChild($this->doc->createElement('ram:ID', $this->invoice->invoice_accounting_account));
            $node->appendChild($nodeAccount);
        }

        return $node;
    } // }}}

    function itemsSubtotalGroupedByTaxPercent()
    { // {{{ 
        $result = [];
        foreach ($this->items as $item) {
            if ($item->item_tax_rate_percent == 0) {
                continue;
            }

            if (!isset($result[$item->item_tax_rate_percent])) {
                $result[$item->item_tax_rate_percent] = 0;
            }
            $result[$item->item_tax_rate_percent] += $item->item_subtotal;
        }
        return $result;
    } // }}} 

    protected function xmlSpecifiedTradeSettlementHeaderMonetarySummation()
    { // {{{ 
        $node = $this->doc->createElement('ram:SpecifiedTradeSettlementHeaderMonetarySummation');
        $node->appendChild($this->currencyElement('ram:LineTotalAmount', $this->invoice->invoice_item_subtotal));
        $node->appendChild($this->currencyElement('ram:ChargeTotalAmount', !empty($this->invoice->invoice_charge_total) ? $this->invoice->invoice_charge_total : 0));
        $node->appendChild($this->currencyElement('ram:AllowanceTotalAmount', !empty($this->invoice->invoice_allowance_total) ? $this->invoice->invoice_allowance_total : 0));
        $node->appendChild($this->currencyElement('ram:TaxBasisTotalAmount', $this->invoice->invoice_item_subtotal));
        $node->appendChild($this->currencyElement('ram:TaxTotalAmount', $this->invoice->invoice_item_tax_total, 2, true));
        $node->appendChild($this->currencyElement('ram:GrandTotalAmount', $this->invoice->invoice_total));
        $node->appendChild($this->currencyElement('ram:TotalPrepaidAmount', $this->invoice->invoice_paid));
        $node->appendChild($this->currencyElement('ram:DuePayableAmount', $this->invoice->invoice_balance));
        return $node;
    } // }}}

    protected function xmlSpecifiedTradeSettlementPaymentMeans($paymentMeans)
    { // {{{
        $node = $this->doc->createElement('ram:SpecifiedTradeSettlementPaymentMeans');
        $node->appendChild($this->doc->createElement('ram:TypeCode', $paymentMeans->typeCode));
        if($paymentMeans->typeCode=='58') {
            $nodePayeePartyCreditorFinancialAccount = $this->doc->createElement('ram:PayeePartyCreditorFinancialAccount');
            $nodePayeePartyCreditorFinancialAccount->appendChild($this->doc->createElement('ram:IBANID', $paymentMeans->ibanID));
            $node->appendChild($nodePayeePartyCreditorFinancialAccount);
        }
        return $node;
    } // }}}
    
    protected function xmlApplicableTradeTax($percent, $subtotal)
    { // {{{
        $node = $this->doc->createElement('ram:ApplicableTradeTax');
        $node->appendChild($this->currencyElement('ram:CalculatedAmount', $subtotal * $percent / 100));
        $node->appendChild($this->doc->createElement('ram:TypeCode', 'VAT'));
        $node->appendChild($this->currencyElement('ram:BasisAmount', $subtotal));
        $node->appendChild($this->doc->createElement('ram:CategoryCode', 'S'));
        $node->appendChild($this->doc->createElement('ram:RateApplicablePercent', $percent));
        return $node;
    } // }}}
    

    protected function dateElement($date)
    { // {{{ 
        $el = $this->doc->createElement('udt:DateTimeString', $this->zugferdFormattedDate($date));
        $el->setAttribute('format', 102);
        return $el;
    } // }}}

    /**
     * @param string $name
     */
    protected function quantityElement($name, $unit_code, $quantity)
    { // {{{
        $el = $this->doc->createElement($name, $this->zugferdFormattedFloat($quantity, 4));
        $el->setAttribute('unitCode', $unit_code);
        return $el;
    } // }}}

    /**
     * @param string $name
     */
    protected function currencyElement($name, $amount, $nb_decimals = 2, $include_currency = false)
    { // {{{
        $el = $this->doc->createElement($name, $this->zugferdFormattedFloat($amount, $nb_decimals));
        if($include_currency)
            $el->setAttribute('currencyID', $this->currencyCode);
        return $el;
    } // }}}
    
    /**
     * @return string|null
     */
    function zugferdFormattedDate($date)
    { // {{{ 
        if ($date && $date <> '0000-00-00') {
            $date = DateTime::createFromFormat('Y-m-d', $date);
            return $date->format('Ymd');
        }
        return '';
    } // }}}

    protected function zugferdFormattedFloat($amount, $nb_decimals = 2)
    { // {{{ 
        return number_format((float)$amount, $nb_decimals);
    } // }}}
}
