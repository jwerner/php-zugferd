<?php

require_once __DIR__ . '/../src/ZugferdXml.php';

// Setup invoice parameters 
$params = [
    // Invoice master data:
    'invoice' => require(__DIR__.'/invoice.php'),
    // Invoice line items:
    'items' => require(__DIR__.'/items.php'),
];
// Currency is within invoice master data, but make it compatible with InvoicePlane ZugferdXml:
$params['currency_code'] = $params['invoice']->invoice_currency_code;

// Create instance:
$zugferd = new ZugferdXml($params);
// Write ZUGFeRD XML to file:
$fp = fopen(__DIR__ . '/factur-x_test.xml', "wb");
fwrite($fp, $zugferd->xml());
fclose($fp);

// Required from InvoicePlane/ZugferdXml:
function htmlsc($output)
{
    // echo 'htmlsc'.PHP_EOL;
    // echo $output.PHP_EOL;
    // echo htmlspecialchars($output, ENT_XML1 && ENT_QUOTES && ENT_SUBSTITUTE).PHP_EOL;
    // echo '---'.PHP_EOL;
    return htmlspecialchars($output, ENT_XML1 && ENT_QUOTES && ENT_SUBSTITUTE);
}
