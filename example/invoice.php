<?php
$json = '{
    "invoice_number": "2020/001",
    "invoice_title": "Baurechnung",
    "invoice_date_created": "2018-04-25",
    "invoice_delivery_date": "2018-03-06",
    "invoice_currency_code": "EUR",
    "invoice_item_subtotal": 252.1,
    "invoice_item_tax_total": 47.9,
    "invoice_total": 300,
    "invoice_charge_total": 0,
    "invoice_allowance_total": 0,
    "invoice_paid": 0,
    "invoice_balance": 300,
    "invoice_terms": "Rapport-Nr.: 42389 vom 08.03.2018\n\nIm 2. OG BT1 Besprechungsraum eine Beamerhalterung an die Decke montiert. Dafür eine Deckenplatte ausgesägt. Beamerhalterung zur Montage auseinander gebaut. Ein Stromkabel für den Beamer, ein HDMI Kabel und ein VGA Kabel durch die Halterung gezogen. Beamerhalterung wieder zusammengebaut und Beamer montiert. Beamer verkabelt und ausgerichtet. Decke geschlossen.",
    "invoice_payment_terms": "Zahlbar sofort rein netto",
    "invoice_accounting_account": "420",
    "user_id": "549910",
    "user_name": "ELEKTRON Industrieservice GmbH",
    "user_description": "Geschäftsführer Egon Schrempp\nAmtsgericht Stuttgart HRB 1234",
    "user_zip": "74465",
    "user_address_1": "Erfurter Strasse 13",
    "user_city": "Demoort",
    "user_country": "DE",
    "user_vat_id": "DE136695976",
    "user_order_reference": "per Mail vom 19.02.2018",
    "user_payment_means": [
        {
            "typeCode": 58, 
            "ibanID": "DE5467894567876500"
        }
    ],
    "client_id": "16259",
    "client_name": "ConsultingService GmbH",
    "client_zip": "76138",
    "client_address_1": "Musterstr. 18",
    "client_city": "Karlsruhe",
    "client_country": "DE",
    "client_reference": "Liselotte Müller-Lüdenscheidt",
    "additional_referenced_documents": [
        { "issuerAssignedID": "13130162", "uriID": "#ef=Aufmass.png", "typeCode": "916" },
        { "issuerAssignedID": "42389", "uriID": "#ef=ElektronRapport_neu-red.pdf", "typeCode": "916" }
    ]
}';
return json_decode($json);
