<?php
$json = '[
    {
        "item_line_id": "01",
        "item_name": "TGA",
        "item_description": "Obermonteur/Monteur",
        "item_tax_rate_percent": 19,
        "item_price": 43.2,
        "item_quantity": "3",
        "item_unit_code": "HUR",
        "item_subtotal": 129.6,
        "item_note": "01 Beamermontage\nFür die doppelte Verlegung, falls erforderlich."
    },
    {
        "item_line_id": "02",
        "item_name": "Beamer-Deckenhalterung",
        "item_description": "-",
        "item_tax_rate_percent": 19,
        "item_price": 122.5,
        "item_quantity": "1",
        "item_unit_code": "H87",
        "item_subtotal": 122.5,
        "item_note": "02 Außerhalb Angebot"
    }]';
return json_decode($json);
